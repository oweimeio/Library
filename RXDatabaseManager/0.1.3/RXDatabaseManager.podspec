#
# Be sure to run `pod lib lint RXDatabaseManager.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RXDatabaseManager'
  s.version          = '0.1.3'
  s.summary          = 'A short description of RXDatabaseManager.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/rlcgroup/rxdbmanager'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'oweimeio' => 'oweimeio@qq.com' }
  s.source           = { :git => 'https://gitlab.com/rlcgroup/rxdbmanager.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'RXDatabaseManager/Classes/**/*'

  s.private_header_files = 'RXDatabaseManager/Classes/Model/*+WCTTableCoding.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'WCDB'
end
